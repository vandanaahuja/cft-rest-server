package com.finxera.service;

import com.finxera.DTO.ClientDTO;
import com.finxera.entites.Clients;

public interface ClientService {
    public ClientDTO getClient(String id);

    public boolean deleteClient(Long id);

    public ClientDTO addClient(Clients clients);
}
