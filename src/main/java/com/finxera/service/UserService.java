package com.finxera.service;

import com.finxera.model.User;

public interface UserService {
    public User getUser(String userName);

    public boolean deleteUser(String userName);

    public User addUser(User user);
}
