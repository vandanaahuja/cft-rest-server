package com.finxera.service.impl;

import com.bancbox.v4.mapper.Mapper;
import com.finxera.DTO.ClientDTO;
import com.finxera.entites.Clients;
import com.finxera.entityManager.GetEntityManager;
import com.finxera.service.ClientService;

public class ClientServiceImpl implements ClientService {
    @Override
    public ClientDTO getClient(String id) {
        try {
            GetEntityManager getEntityManager =  new GetEntityManager();
            Clients clients = (Clients) getEntityManager.getEntityManger().find(Clients.class, id);
            ClientDTO clientDTO = new ClientDTO();
            clientDTO = (ClientDTO) Mapper.customMapper(clientDTO, clients);
            return clientDTO;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteClient(Long id) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ClientDTO addClient(Clients clients) {
        // TODO Auto-generated method stub
        return null;
    }
}
