package com.finxera.service.impl;

import java.util.HashMap;

import com.finxera.model.User;
import com.finxera.service.UserService;

public class UserServiceImpl implements UserService {
    static HashMap<String, User> users = new HashMap<>();

    @Override
    public User getUser(String userName) {
        return users.get(userName);
    }

    @Override
    public boolean deleteUser(String userName) {
        if (users.containsKey(userName)) {
            users.remove(userName);
            return true;
        }
        return false;
    }

    @Override
    public User addUser(User user) {
        return users.put(user.getUserName(), user);
    }
}
