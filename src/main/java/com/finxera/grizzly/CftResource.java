package com.finxera.grizzly;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bancbox.v4.da.transaction.entity.mapper.BancboxEntityMapper;
import com.finxera.DTO.ClientDTO;
import com.finxera.model.User;
import com.finxera.service.ClientService;
import com.finxera.service.UserService;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("cftResource")
public class CftResource {
    private UserService userService;
    private ClientService clientService;

    /*
     * @Inject public MyResource(UserService userService) { this.userService =
     * userService; }
     */
    @Inject
    public CftResource(ClientService clientService) {
        this.clientService = clientService;
    }

    /**
     * Method handling HTTP GET requests. The returned object will be sent to
     * the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Got it!";
    }

    @GET
    @Path("/getUser")
    @Produces(MediaType.TEXT_PLAIN)
    public String getUser(@PathParam("userName") String userName) {
        User user = userService.getUser(userName);
        if (user != null) {
            return "Found";
        }
        return "Not Found";
    }

    @GET
    @Path("get/User/{userName}")
    @Produces(MediaType.APPLICATION_JSON)
    public User get(@PathParam("userName") String userName) {
        User user = userService.getUser(userName);
        return user;
    }

    @PUT
    @Path("put/User")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String putUser(User user) {
        userService.addUser(user);
        return "Added Successfully";
    }

    @GET
    @Path("get/Client/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getClient(@PathParam("id") String id) {
        ClientDTO client = clientService.getClient(id);
        BancboxEntityMapper instance = BancboxEntityMapper.getInstance();
        return instance.mapEntityToJson(client);
    }
//    @PUT
//    @Path("put/Client")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    public ClientDTO putClient(ClientDTO clientDTO) {
//    	return clientDTO;
//    }
}
