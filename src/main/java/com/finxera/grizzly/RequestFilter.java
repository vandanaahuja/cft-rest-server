package com.finxera.grizzly;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;


public class RequestFilter implements ContainerRequestFilter {
	 
	    @Override
	    public void filter(ContainerRequestContext requestContext)
	                    throws IOException {
	    	System.out.println("in request filter...");
	    	
	    	String userName = requestContext.getUriInfo().getQueryParameters().getFirst("UserName");
	    		  if (userName == null || "".equals(userName)) {
	    		   System.out.println("Authentication Filter Failed");
	    		   ResponseBuilder responseBuilder = Response.serverError();
	    		   Response response = responseBuilder.status(Status.BAD_REQUEST).build();
	    		   requestContext.abortWith(response);
	    		  } 
	    		  else {
	    			  System.out.println("Authentication Filter Passed; UserName is " + userName);  
	    		  }
	    		   
	    	
	        /*
	         final SecurityContext securityContext =
	                    requestContext.getSecurityContext();
	        	Principal principal = securityContext.getUserPrincipal();
	        if (securityContext == null ||
	                    !securityContext.isUserInRole("privileged")) {
	 
	                requestContext.abortWith(Response
	                    .status(Response.Status.UNAUTHORIZED)
	                    .entity("User cannot access the resource.")
	                    .build());
	        }*/
	    }
	}
