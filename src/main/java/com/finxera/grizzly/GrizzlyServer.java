package com.finxera.grizzly;

import java.net.URI;

import javax.xml.ws.Service;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import com.finxera.guice.GuiceFeature;

public class GrizzlyServer implements Runnable {

	private HttpServer server;
	private static GrizzlyServer self = null;
	private static String BASE_URI = null;

	private GrizzlyServer() {
		BASE_URI = Configuration.getInstance("server").getProperty("APIServer.BIND_PROTOCOL").toString() + "://"
				+ Configuration.getInstance("server").getProperty("APIServer.BIND_IP").toString() + ":"
				+ String.valueOf(
						Integer.parseInt(Configuration.getInstance("server").getProperty("APIServer.BIND_PORT")))
				+ "/CFTRest/";

	}

	public static GrizzlyServer getInstance() {
		if (self != null) {
			return self;
		}
		self = new GrizzlyServer();
		return self;
	}

	@Override
	public void run() {
		startServer();
	}

	/**
	 * Starts Grizzly HTTP server exposing JAX-RS resources defined in this
	 * application
	 */
	private HttpServer startServer() {
		final ResourceConfig rc = new ResourceConfig().packages("com.finxera.grizzly");
		// rc.registerClasses(RequestFilter.class, ResponseFilter.class);
		rc.register(GuiceFeature.class);
		HttpServer server = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
		System.out
				.println(String.format("Jersey app started with WADL available at " + "%sapplication.wadl", BASE_URI));
		return server;

	}

	public void shutdown() {
		server.shutdownNow();

	}

}
