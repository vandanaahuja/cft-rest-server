package com.finxera.grizzly;

	import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
	 
	public class ResponseFilter implements ContainerResponseFilter {
	 
	    @Override
	    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
	        throws IOException {
	 
	    	System.out.println("in response filter...");
	            responseContext.getHeaders().add("X-Powered-By", "Jersey :-)");
	            System.out.println("message = " + responseContext.getEntity().toString());
	    }
	}
