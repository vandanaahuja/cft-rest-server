package com.finxera.grizzly;

import java.io.IOException;

import com.bancbox.v4.config.ArgsEM;
import com.bancbox.v4.config.ConfiguartionLoader;
import com.beust.jcommander.JCommander;

public class CFTMain {
	private static GrizzlyServer grizzlyServer;

	private static void getEntityMangerConfig(String[] args) {
		ArgsEM argsEM = new ArgsEM();
		new JCommander(argsEM, args);
		ConfiguartionLoader.loadConfiguration();
	}

	/**
	 * Main method.
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		getEntityMangerConfig(args);
		grizzlyServer = GrizzlyServer.getInstance();
		new Thread(grizzlyServer).run();

		try {
			Thread.currentThread().join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		grizzlyServer.shutdown();

	}
}
