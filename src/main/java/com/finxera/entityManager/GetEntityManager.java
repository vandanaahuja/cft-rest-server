package com.finxera.entityManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.bancbox.v4.da.transaction.EMFactory;
import com.bancbox.v4.managers.TransactionManager.ExecutionMode;
import com.bancbox.v4.managers.TransactionManager.TransactionMode;

public class GetEntityManager {
    EntityManagerFactory emFactory = new EMFactory(TransactionMode.write, ExecutionMode.PASSIVE);
    EntityManager entityManager = emFactory.createEntityManager();

    public EntityManager getEntityManger() {
        return entityManager;
    }
}
