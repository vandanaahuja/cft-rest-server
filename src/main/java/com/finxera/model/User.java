package com.finxera.model;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;



//@JsonIgnoreProperties(ignoreUnknown = true, value = { "lastName" }) 
public class User implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5065958710344056228L;
	private String userName;
	private String firstName;
	@JsonIgnore
	private String lastName;
	public User() {
	}
	
	public User(String userName, String firstName, String lastName) {
		super();
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	//@JsonIgnore
//	@JsonIgnore
	public String getLastName() {
		return lastName;
	}
	//@JsonProperty
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
